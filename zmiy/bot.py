import django
import os
import telebot
import uuid

from zmiy.models import Code
from zmiy.Parser.main import translater
os.environ["DJANGO_SETTINGS_MODULE"] = 'epta_zmeika.settings'
django.setup()


token = "token"

# Обходим блокировку с помощью прокси
telebot.apihelper.proxy = {'https': 'socks5h://geek:socks@t.geekclass.ru:7777'}

# подключаемся к телеграму
bot = telebot.TeleBot(token=token)

# content_types=['text'] - сработает, если нам прислали текстовое сообщение
@bot.message_handler(content_types=['text'])
def echo(message):
    # message - входящее сообщение
    # message.text - это его текст
    # message.chat.id - это номер его автора
    text = message.text
    user = message.chat.id

    code = Code()
    code.code_text = text
    code.save()

    name = 'static/' + str(uuid.uuid4()) + '.txt'

    # создаем файл и записываем туда данные
    with open(name, 'w', encoding='utf-8') as new_file:
        new_file.write(code.code_text)
        new_file.close()

    translater(name)

    new_name = name[7:-3] + 'py'
    path = f'{os.getcwd()}//static'
    os.system(f'cd /')
    os.system(f'cd {path}')
    os.system(f'python {new_name} > output.txt')

    path = f"{os.getcwd()}\\static\\"
    # path = r"C:\\Users\\User\\PycharmProjects\\zmeuka\\static\\"

    os.system(f'python {path + new_name} > output.txt')

    with open('output.txt', 'r', encoding='utf-8') as new_file:
        output = new_file.read()

    bot.send_message(user, output)


# поллинг - вечный цикл с обновлением входящих сообщений
bot.polling(none_stop=True)
