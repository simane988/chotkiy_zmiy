# import some cool stuff
import re
import os
import zmiy.Parser.dictionary as dictionary


def translater(chotky_path):
    # here we are open our dictionary
    my_dictionary = dictionary.Dict

    # and here we are open ЧоткийЗмiй code and copy
    zmiy_code = str(open(chotky_path, 'r', encoding='utf-8').read())

    for chotky_word, word in my_dictionary.items():
        zmiy_code = re.sub(str(chotky_word), str(word), zmiy_code)

    python_code = open('.'.join(chotky_path.split('.')[:-1])+'.py', 'w', encoding='utf-8')
    python_code.write(zmiy_code)
    python_code.close()


if __name__ == '__main__':
    translater('input()')
