# Чоткий змiй  
Чоткий змiй — это набор ПО, который создан для взращивания интереса к программированию   
и помощи с его изучением особой прослойке населения, проще говоря, гопникам и им подобным, на 
понятном им языке. 

---

В данном наборе ПО присутствуют:  
1. IDE для написания чоткого кода  
2. Переводчик с чоткого(пацанского) языка на python
3. Telegram бот для выполнения кода прямо в Telegram

Наш проект opensource (бесплатный), благодаря этому каждый может использовать его в своих целях
и настроить его под себя.

